library form_validator;

import 'package:flutter/material.dart';
import '../form_validator.dart';

/// Widget permettant d'utiliser moins de boilerplate.
/// Pourrait très bien s'en passer un utliser un `StreamBuilder` à la place
/// NB: ce widget ne ferme pas le bloc.
class FormValidatorBuilder extends StatelessWidget {

  final FormValidatorBloc bloc;
  final Widget Function(BuildContext context, FormValidatorData data) builder;

  FormValidatorBuilder({
    required this.bloc,
    required this.builder
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<FormValidatorData>(
      initialData: bloc.data,
      stream: bloc.listen,
      builder: (context, snapshot) => builder(context, snapshot.data!)
    );
  }
}