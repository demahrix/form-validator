

class ValidatorFunc {

  ValidatorFunc._();

  static String? Function(T, Map<String, dynamic>, bool) required<T>(String message) {
    return (T value, Map<String, dynamic> values, bool submitted) {
      if (!submitted)
      return null;
      if (value == null)
        return message;
      if (value is String && (value == '' || value.trim() == ''))
        return message;
      return null;
    };
  }

  static String? Function(Iterable<T>, Map<String, dynamic>, bool) notEmpty<T>(String message) {
    return (Iterable<T> value, Map<String, dynamic> values, bool submitted, ) {
      if (!submitted)
        return null;
      return value.isEmpty ? message : null;
    };
  }

  // multiple function

}



// bool isNotEmpty<T>(T value) {
//   if (value == null)
//     return false;

//   if (value is String && (value == "" || value.trim() == ""))
//     return false;

//   return true;
// }

/* *************************************************************************** */
/* *************************************************************************** */
/* *************************** STRING FUNCTION ******************************* */
/* *************************************************************************** */
/* *************************************************************************** */

// bool isNonNull(Object? value) {
//   return value != null;
// }

// bool isEmail(String value) {
//   throw UnimplementedError();
// }

/* *************************************************************************** */
/* *************************************************************************** */
/* *************************** NUMBER FUNCTION ******************************* */
/* *************************************************************************** */
/* *************************************************************************** */


// bool isNumber(String value) {
//   throw UnimplementedError();
// }

// bool isLongThan(String value) {
//   throw UnimplementedError();
// }

// bool isShortThen(String value) {
//   throw UnimplementedError();
// }

// bool isSmallThan(String value, String limit) {
//   throw UnimplementedError();
// }

// bool isGreatThan(String value, String max) {
//   throw UnimplementedError();
// }

// bool between<T extends num>(T value, T min, T max) {
//   return value >= min && value <= max;
// }
