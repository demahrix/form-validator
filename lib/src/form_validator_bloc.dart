library form_validator;

import 'dart:async';
import 'dart:collection' show HashMap;
import 'package:form_validator/src/validator_field.dart';

class FormValidatorData {

  HashMap<String, ValidatorField> fields;
  bool isValid;
  bool isLoading;

  FormValidatorData({
    required this.fields,
    this.isValid = true,
    this.isLoading = false
  });

}

class FormValidatorBloc {

  FormValidatorData _data;

  /// Nous permet de savoir si on a déjà tenter d'envoyer le formulaire une fois
  /// pour pouvoir activer la validation quand le champ est obligation
  bool _submitted = false;

  final StreamController<FormValidatorData> _controller = StreamController();

  FormValidatorBloc({
    required FormValidatorData data
  }): _data = data;

  FormValidatorBloc.fromMap(Map<String, ValidatorField> fields): this(data: FormValidatorData(fields: HashMap.from(fields)));

  Stream<FormValidatorData> get listen => _controller.stream;
  FormValidatorData get data => _data;
  bool get submitted => _submitted;

  Map<String, dynamic> getFormValues() => _data.fields.map<String, dynamic>((key, value) => MapEntry(key, value.value));

  void changeValue(String fieldKey, dynamic value, { bool notify = true }) {
    ValidatorField? field = _data.fields[fieldKey];
    if (field == null)
      throw Exception("field with key: $fieldKey not found");
    field.value = value;

    if (notify)
      _verifyAllFields(_submitted);
  }

  void changeValues(Map<String, dynamic> values, { bool notify = true }) {
    for (var entry in values.entries) {
      ValidatorField? field = _data.fields[entry.key];
      if (field == null)
        throw Exception("field with key: ${entry.key} not found");
      field.value = entry.value;
    }
    if (notify)
      _verifyAllFields(_submitted);
  }

  /// Permet d'envoyer une erreur sur un champs \
  /// `Exepmle`: Apres avoir verifier un constater un nom d'utilisateur est deja pris 
  void setError(String fieldKey, String errorMessage) {
    ValidatorField? field = _data.fields[fieldKey];

    if (field == null)
      throw Exception("field key: $fieldKey not found");

    field.setErrorMessage(errorMessage);

    bool isValid = false;
    final values = getFormValues();

    for (final field in _data.fields.values) {
      /// Evite que son erreur soit reecrit
      if (field == field)
        continue;

      String? error = field.verify(_submitted, values);
      if (isValid && error != null)
        isValid = false;
    }

    _data.isValid = isValid;
    _controller.sink.add(_data);
  }

  /// Provoque un rechargement du `streamBuilder`
  /// utile dans cas au on change la valeur d'une reference
  void reload() {
    _controller.sink.add(_data);
  }

  void setLoadingStatus(bool status) {
    _data.isLoading = status;
    _controller.sink.add(_data);
  }

  bool validate() {
    if (!_submitted)
      _submitted = true;
    return _verifyAllFields(_submitted);
  }

  /// [alreadySubmitted] pour savoir si le formualire a deja ete envoye au moins une fois
  bool _verifyAllFields(bool alreadySubmitted) {

    bool isValid = true;
    final values = getFormValues();

    for (final field in _data.fields.values) {
      String? error = field.verify(alreadySubmitted, values);
      if (isValid && error != null)
        isValid = false;
    }

    _data.isValid = isValid;
    _controller.sink.add(_data);
    return isValid;
  }

  void reset() {
    throw UnimplementedError();
  }

  bool get isValid => _data.isValid;

  void dispose() {
    _controller.close();
  }

}
