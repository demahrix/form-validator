
typedef _ValidatorFunction<T> = String? Function(T value, Map<String, dynamic> values, bool submitted);

class ValidatorField<T> {

  /// La valuer actuelle du champs
  T value;

  /// Le libellé de erreur courante, si vaut `null` il y pas d'erreur
  String? _errorMessage;

  /// La fonction qui permettant de valider le champ
  final _ValidatorFunction<T>? validator;

  ValidatorField(this.value, {
    String? initialErrorMessage,
    this.validator
  }): _errorMessage = initialErrorMessage;

  // FIXME Ne pas faire de `_errorMessage` un membre private car il a un [getter] et un [setter]
  void setErrorMessage(String message) {
    _errorMessage = message;
  }

  String? get errorMessage => _errorMessage;
  // bool get isRequired => requiredMessage != null;

  // Retourne [null] si le champ est valide sinon retourne un messge d'erreur
  // FIXME: essayer d'optimiser cette function
  String? verify(bool submitted, Map<String, dynamic> values) {
    if (validator != null) {
      final String? currentErrorMessage = validator!(value, values, submitted);
      if (currentErrorMessage != null)
        return _errorMessage = currentErrorMessage;
    }
    return _errorMessage = null;
  }

}
